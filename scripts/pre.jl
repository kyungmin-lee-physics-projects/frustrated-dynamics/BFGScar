using DrWatson
@quickactivate "BFGScar"

using BFGScar
using LatticeTools

# using Plots
using PyPlot
using ITensors
using JLD2


function prepare_pinwheel(kagome)
    hc0 = Hypercube([2 0; 0 2])
    @assert all(let
        R, r = hc0.wrap(col)
        iszero(r)
    end for col in eachcol(kagome.lattice.hypercube.shape_matrix))
   
    pinwheel_phase_map = Dict(
        [0, 0] => 1,
        [1, 0] => 2,
        [0, 1] => 3,
        [1, 1] => 4,
    )

    pinwheel_motif = Dict(
        (1, "A") => 1, # bb
        (1, "B") => 1, # bb
        (1, "C") => 1, # bb
        (2, "A") => 2, # wb
        (2, "B") => 2,
        (2, "C") => 1,
        (3, "A") => 2,
        (3, "B") => 1,
        (3, "C") => 2, # wb
        (4, "A") => 1, 
        (4, "B") => 2, # wb
        (4, "C") => 2, 
    )

    spin_config = zeros(Int64, length(kagome.lattice.supercell.sites))

    for (isite, ((sitetype, siteucindex), sitecoord)) in enumerate(kagome.lattice.supercell.sites)
        _, R = hc0.wrap(siteucindex)
        p = pinwheel_phase_map[R]
        spin_config[isite] = pinwheel_motif[(p, sitetype)]
    end
    return spin_config
end


function draw_lattice(kagome, spin_config)
   
    xs = Float64[]
    ys = Float64[]

    for (isite, ((sitetype, siteucindex), sitecoord)) in enumerate(kagome.lattice.supercell.sites)
        x, y = fract2carte(kagome.lattice.supercell, sitecoord)
        push!(xs, x)
        push!(ys, y)
    end

    plot(size=(600, 600))
    # scatter!(xs[spin_config .== 0], ys[spin_config .== 0], color="white", markersize=8)
    scatter!(xs[spin_config .== 1], ys[spin_config .== 1], color="white", markersize=8)
    scatter!(xs[spin_config .== 2], ys[spin_config .== 2], color="black", markersize=8)
    # xlims!(0, 6)
    # ylims!(0, 6)
end

function measure_Sz(psi, n)
    psi = orthogonalize(psi, n)
    sn = siteind(psi, n)
    Sz = scalar(dag(prime(psi[n], "Site")) * op("Sz", sn) * psi[n])
    return real(Sz)
end    


# http://itensor.org/docs.cgi?vers=julia&page=getting_started/mps_time_evolution

function main()
    # shape = [2 2; -2 4]
    # shape = [2 0; 0 2]
    shape = [4 0; 0 4]

    kagome = BFGScar.make_kagome_lattice(shape)
    spin_config = prepare_pinwheel(kagome)
    # spin_config = [1, 1, 1, 2, 2, 2, 1, 1, 1, 2, 2, 2]
    # draw_lattice(kagome, spin_config)

    nsites = sitecount(kagome.lattice.supercell)

    sites = siteinds("S=1/2", nsites; conserve_qns=true)
    ampo = AutoMPO()
    J = 0.01
    τ = 0.01
    gates = ITensor[]
    for hexa in kagome.motifs.hexagons
        for i1 in 1:6, i2 in (i1+1):6
            j1, R1 = hexa[i1]
            j2, R2 = hexa[i2]
            if mod(i1, 6) + 1 == i2 || mod(i2, 6) + 1 == i1
                push!(nnbonds, (j1, j2, R1, R2, :Jxyz))
                # h = (J * 0.5) * op("S+", sites[j1]) * op("S-", sites[j2]) +
                #     (J * 0.5) * op("S-", sites[j1]) * op("S+", sites[j2]) +
                #                 op("Sz", sites[j1]) * op("Sz", sites[j2])
            else
                push!(bonds, (j1, j2, R1, R2, :Jz))
                # h = op("Sz", sites[j1]) * op("Sz", sites[j2])
            end
            # G = exp(-1im * 0.5 * τ * h)
            # push!(gates, G)
        end
    end

    bond_groups = []
    while !isempty(nnbonds) || !isempty(bonds)
        visited_sites = Set{Int}()
        bond_group = []
        if !isempty(nnbonds)
            kept_bonds = trues(length(nnbonds))
            for (ibond, (i1, i2, R1, R2, bondtype)) in enumerate(nnbonds)
                if i1 ∉ visited_sites && i2 ∉ visited_sites
                    push!(visited_sites, i1)
                    push!(visited_sites, i2)
                    kept_bonds[ibond] = false
                    push!(bond_group, (i1, i2, R1, R2, bondtype))
                end
            end
            nnbonds = nnbonds[kept_bonds]
        end

        if !isempty(bonds)
            kept_bonds = trues(length(bonds))
            for (ibond, (i1, i2, R1, R2, bondtype)) in enumerate(bonds)
                if i1 ∉ visited_sites && i2 ∉ visited_sites
                    push!(visited_sites, i1)
                    push!(visited_sites, i2)
                    kept_bonds[ibond] = false
                    push!(bond_group, (i1, i2, R1, R2, bondtype))
                end
            end
            bonds = bonds[kept_bonds]
        end
        push!(bond_groups, bond_group)
    end
   
    xs = Float64[]
    ys = Float64[]

    for (isite, ((sitetype, siteucindex), sitecoord)) in enumerate(kagome.lattice.supercell.sites)
        x, y = fract2carte(kagome.lattice.supercell, sitecoord)
        push!(xs, x)
        push!(ys, y)
    end


    #=
    fig = PyPlot.figure(figsize=(6, 6))
    for (ibg, bg) in enumerate(bond_groups)
        fig.clf()
        ax = fig.gca()
        # fig = plot(size=(600, 600))
        # scatter!(xs[spin_config .== 0], ys[spin_config .== 0], color="white", markersize=8)
        # ax.plot(xs[spin_config .== 1], ys[spin_config .== 1], color="white", markersize=8)
        # ax.plot(xs[spin_config .== 2], ys[spin_config .== 2], color="black", markersize=8)
        ax.plot(xs, ys, "ko")
        for bond in bg
            i1, i2, R1, R2, _ = bond
            r1 = fract2carte(kagome.lattice.supercell, getsitecoord(kagome.lattice.supercell, i1) + R1)
            r2 = fract2carte(kagome.lattice.supercell, getsitecoord(kagome.lattice.supercell, i2) + R2)
            ax.plot([r1[1], r2[1]], [r1[2], r2[2]], "k-")
        end
        ax.set_aspect(1.0)
        fig.savefig("bg-$ibg.png", dpi=100, bbox_inches="tight")
        # savefig(fig, "bg-$ibg.png")
        # xlims!(0, 6)
        # ylims!(0, 6)
    end
    exit()
    =#

    # ITensor part


    sites = siteinds("S=1/2", nsites; conserve_qns=true)

    hamiltonian_groups = Vector{ITensor}[]
    for bg in bond_groups
        H = ITensor[]
        for (i1, i2, R1, R2, bt) in bg
            # @show i1, i2
            push!(H,
                0.5 * op("S+", sites[i1]) * op("S-", sites[i2]) +
                0.5 * op("S-", sites[i1]) * op("S+", sites[i2]) +
                      op("Sz", sites[i1]) * op("Sz", sites[i2])
            )
        end
        push!(hamiltonian_groups, H)
    end

    J = 0.1
    Δτ = 0.1

    gate_groups = [
        exp.((-0.5 * im * J * Δτ) .* hg)
        for hg in hamiltonian_groups
    ]
    gates = vcat(gate_groups...)
    append!(gates, reverse(gates))

    state = [spin_config[i] == 1 ? "Up" : "Dn" for i in 1:nsites]
    psi0 = productMPS(sites, state)
    # psi0 = randomMPS(sites, state)

    psi = psi0
    open("output.tsv", "w") do io
        Szs = [measure_Sz(psi, i) for i in 1:nsites]
        print(io, 0)
        print(0)
        for Sz in Szs
            print(io, "\t", Sz)
            print("\t", Sz)
        end
        println(io, "\t", maxlinkdim(psi))
        println("\t", maxlinkdim(psi))

        for step in 1:100
            psi = apply(gates, psi; cutoff=1E-12, maxdim=10000)
            Szs = [measure_Sz(psi, i) for i in 1:nsites]
            print(io, τ * step)
            print(τ * step)
            for Sz in Szs
                print(io, "\t", Sz)
                print("\t", Sz)
            end
            println(io, "\t", maxlinkdim(psi))
            println("\t", maxlinkdim(psi))
        end
        flush(stdout)
        flush(io)
    end
    JLD2.@save "final_state.jld2" sites psi

    # sweeps = Sweeps(10)
    # maxdim!(sweeps, 20, 60, 100, 100, 200, 400, 800)
    # cutoff!(sweeps, 1E-8)
    # @show sweeps

    # observer = DMRGObserver(["Sz",], sites; energy_tol=2*eps(Float64))

    # @info "Starting"
    # dmrg(H, psi0, sweeps; verbose=true, observer=observer)
    # @info "Finished"

    # Szs = measurements(observer)["Sz"][end]
    # @show Szs
    # # val = scalar(psi * op(siteind(psi, 1), "Sz") * dag(prime(psi)))
    # return Szs

    # ms = abs.(Szs) ./ maximum(abs.(Szs)) 

    # xs = Float64[]
    # ys = Float64[]

    # for (isite, ((sitetype, siteucindex), sitecoord)) in enumerate(kagome.lattice.supercell.sites)
    #     x, y = fract2carte(kagome.lattice.supercell, sitecoord)
    #     push!(xs, x)
    #     push!(ys, y)
    # end

    # plot(size=(600, 600))
    # # scatter!(xs[spin_config .== 0], ys[spin_config .== 0], color="white", markersize=8)
    # scatter!(xs[spin_config .== 1], ys[spin_config .== 1], color="white", markersize=8 .* ms)
    # scatter!(xs[spin_config .== 2], ys[spin_config .== 2], color="black", markersize=8 .* ms)
    # # xlims!(0, 6)
    # # ylims!(0, 6)
end

ret = main()
