using DrWatson
@quickactivate "BFGScar"

using BFGScar
using LatticeTools

# using Plots
using ProgressMeter
using ITensors

struct Tee{TIO <: Tuple} <: IO
    streams::TIO
end

Tee(streams...) = Tee(streams)

function _do_tee(tee, f, xs...)
    for io in tee.streams
        f(io, xs...)
    end
end

Base.write(tee::Tee, x) = _do_tee(tee, write, x)
Base.write(tee::Tee, x::Union{SubString{String},String}) = _do_tee(tee, write, x)

function prepare_pinwheel(kagome)
    hc0 = Hypercube([2 0; 0 2])
    @assert all(let
        R, r = hc0.wrap(col)
        iszero(r)
    end for col in eachcol(kagome.lattice.hypercube.shape_matrix))
   
    pinwheel_phase_map = Dict(
        [0, 0] => 1,
        [1, 0] => 2,
        [0, 1] => 3,
        [1, 1] => 4,
    )

    pinwheel_motif = Dict(
        (1, "A") => 1, # bb
        (1, "B") => 1, # bb
        (1, "C") => 1, # bb
        (2, "A") => 2, # wb
        (2, "B") => 2,
        (2, "C") => 1,
        (3, "A") => 2,
        (3, "B") => 1,
        (3, "C") => 2, # wb
        (4, "A") => 1, 
        (4, "B") => 2, # wb
        (4, "C") => 2, 
    )

    spin_config = zeros(Int64, length(kagome.lattice.supercell.sites))

    for (isite, ((sitetype, siteucindex), sitecoord)) in enumerate(kagome.lattice.supercell.sites)
        _, R = hc0.wrap(siteucindex)
        p = pinwheel_phase_map[R]
        spin_config[isite] = pinwheel_motif[(p, sitetype)]
    end
    return spin_config
end


function draw_lattice(kagome, spin_config)
   
    xs = Float64[]
    ys = Float64[]

    for (isite, ((sitetype, siteucindex), sitecoord)) in enumerate(kagome.lattice.supercell.sites)
        x, y = fract2carte(kagome.lattice.supercell, sitecoord)
        push!(xs, x)
        push!(ys, y)
    end

    plot(size=(600, 600))
    # scatter!(xs[spin_config .== 0], ys[spin_config .== 0], color="white", markersize=8)
    scatter!(xs[spin_config .== 1], ys[spin_config .== 1], color="white", markersize=8)
    scatter!(xs[spin_config .== 2], ys[spin_config .== 2], color="black", markersize=8)
    # xlims!(0, 6)
    # ylims!(0, 6)
end

function measure_Sz!(psi, n)
    psi = orthogonalize(psi, n)
    sn = siteind(psi, n)
    Sz = scalar(dag(prime(psi[n], "Site")) * op("Sz", sn) * psi[n])
    return real(Sz)
end    


function measure_entropy!(psi::MPS, b::Integer)
    orthogonalize!(psi, b)
    l = linkind(psi, b-1)
    if isnothing(l)
        U, S, V = svd(psi[b], siteind(psi, b))
    else
        U, S, V = svd(psi[b], (l, siteind(psi, b)))
    end

    SvN = 0.0
    for n in 1:dim(S, 1)
        p = abs2(S[n, n])
        SvN -= p * log(p)
    end
    return SvN
end


function measure_Sz!(psi::MPS)
    sites = siteinds(psi)
    spins = Vector{Float64}(undef, length(sites))
    for (i, s) in enumerate(sites)
        Sz = op(s, "Sz")
        orthogonalize!(psi, i)
        spins[i] = real(scalar(dag(prime(psi[i], "Site")) * Sz * psi[i]))
    end
    return spins
end

# http://itensor.org/docs.cgi?vers=julia&page=getting_started/mps_time_evolution

function main()
    shape = [2 0; 0 2]

    kagome = BFGScar.make_kagome_lattice(shape)
    spin_config = prepare_pinwheel(kagome)

    nsites = sitecount(kagome.lattice.supercell)

    nnbonds = []
    bonds = []
    for hexa in kagome.motifs.hexagons
        for i1 in 1:6, i2 in (i1+1):6
            j1, R1 = hexa[i1]
            j2, R2 = hexa[i2]
            if mod(i1, 6) + 1 == i2 || mod(i2, 6) + 1 == i1
                push!(nnbonds, (j1, j2, R1, R2, :Jxyz))
            else
                push!(bonds, (j1, j2, R1, R2, :Jz))
            end
        end
    end

    bond_groups = []
    while !isempty(nnbonds) || !isempty(bonds)
        visited_sites = Set{Int}()
        bond_group = []
        if !isempty(nnbonds)
            kept_bonds = trues(length(nnbonds))
            for (ibond, (i1, i2, R1, R2, bondtype)) in enumerate(nnbonds)
                if i1 ∉ visited_sites && i2 ∉ visited_sites
                    push!(visited_sites, i1)
                    push!(visited_sites, i2)
                    kept_bonds[ibond] = false
                    push!(bond_group, (i1, i2, R1, R2, bondtype))
                end
            end
            nnbonds = nnbonds[kept_bonds]
        end

        if !isempty(bonds)
            kept_bonds = trues(length(bonds))
            for (ibond, (i1, i2, R1, R2, bondtype)) in enumerate(bonds)
                if i1 ∉ visited_sites && i2 ∉ visited_sites
                    push!(visited_sites, i1)
                    push!(visited_sites, i2)
                    kept_bonds[ibond] = false
                    push!(bond_group, (i1, i2, R1, R2, bondtype))
                end
            end
            bonds = bonds[kept_bonds]
        end
        push!(bond_groups, bond_group)
    end
   
    # xs = Float64[]
    # ys = Float64[]
    # for (isite, ((sitetype, siteucindex), sitecoord)) in enumerate(kagome.lattice.supercell.sites)
    #     x, y = fract2carte(kagome.lattice.supercell, sitecoord)
    #     push!(xs, x)
    #     push!(ys, y)
    # end

    # ITensor part

    sites = siteinds("S=1/2", nsites; conserve_qns=true)

    hamiltonian_groups = Vector{ITensor}[]
    for bg in bond_groups
        H = ITensor[]
        for (i1, i2, R1, R2, bt) in bg
            push!(H,
                0.5 * op("S+", sites[i1]) * op("S-", sites[i2]) +
                0.5 * op("S-", sites[i1]) * op("S+", sites[i2]) +
                      op("Sz", sites[i1]) * op("Sz", sites[i2])
            )
        end
        push!(hamiltonian_groups, H)
    end

    J = 0.1
    Δτ = 0.1

    gate_groups = [
        exp.((-0.5 * im * J * Δτ) .* hg)
        for hg in hamiltonian_groups
    ]
    gates = vcat(gate_groups...)
    
    revgates = reverse(gates)
    append!(revgates, gates)

    append!(gates, reverse(gates))

    state = [spin_config[i] == 1 ? "Up" : "Dn" for i in 1:nsites]
    psi0 = productMPS(sites, state)

    psi = psi0

    let 
        phi1 = apply(gates, psi; cutoff=1E-12, maxdim=10000)
        phi2 = apply(revgates, psi; cutoff=1E-12, maxdim=10000)
        @show 1 - abs2(dot(phi1, phi2))
        flush(stdout)
    end

    open("output.csv", "w") do io
        print(io, 0)
        print(io, ",", abs2(dot(psi0, psi)))
        print(io, ",", maxlinkdim(psi))
        print(io, ",", measure_entropy!(psi, nsites÷2))
        sp = measure_Sz!(psi)
        for i in 1:nsites
            print(io, ",", sp[i])
        end
        println(io)
        flush(io)

        @showprogress for step in 1:500
            psi = apply(gates, psi; cutoff=1E-12, maxdim=10000)
            print(io, Δτ * step)
            print(io, ",", abs2(dot(psi0, psi)))
            print(io, ",", maxlinkdim(psi))
            print(io, ",", measure_entropy!(psi, nsites÷2))
            sp = measure_Sz!(psi)
            for i in 1:nsites
                print(io, ",", sp[i])
            end
            println(io)
            flush(io)
        end
    end
end

ret = main()