using DrWatson

@quickactivate "BFGScar"

using CSV
using DataFrames
using Plots
using ArgParse

df = CSV.read("output.csv", DataFrame)