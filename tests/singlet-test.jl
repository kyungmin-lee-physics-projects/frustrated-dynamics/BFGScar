using ITensors


function measure_entropy!(psi::MPS, b::Integer)
    orthogonalize!(psi, b)
    l = linkind(psi, b-1)
    if isnothing(l)
        U, S, V = svd(psi[b], siteind(psi, b))
    else
        U, S, V = svd(psi[b], (l, siteind(psi, b)))
    end

    SvN = 0.0
    for n in 1:dim(S, 1)
        p = abs2(S[n, n])
        SvN -= p * log(p)
    end
    return SvN
end

function measure_spin!(psi)
    sites = siteinds(psi)
    spins = Matrix{Float64}(undef, (3, length(sites)))
    for (i, s) in enumerate(sites)
        Sx = op(s, "Sx")
        Sy = op(s, "Sy")
        Sz = op(s, "Sz")
        orthogonalize!(psi, i)
        spins[1, i] = real(scalar(dag(prime(psi[i], "Site")) * Sx * psi[i]))
        spins[2, i] = real(scalar(dag(prime(psi[i], "Site")) * Sy * psi[i]))
        spins[3, i] = real(scalar(dag(prime(psi[i], "Site")) * Sz * psi[i]))
    end
    return spins
end

function main()
    sites = siteinds("S=1/2", 2)
    H = let
        H = AutoMPO()
        # H += -1, "Sz", 1, "Sz", 2
        # H += -0.5, "S+", 1, "S-", 2
        # H += -0.5, "S-", 1, "S+", 2
        H += -sin(pi/6), "Sx", 1
        H += -cos(pi/6), "Sy", 1
        H += -0.01, "Sx", 2
        MPO(H, sites)
    end

    state = ["Up", "Dn"]
    psi0 = productMPS(sites, state)

    sweeps = Sweeps(6)
    maxdim!(sweeps, 10, 10, 10)
    cutoff!(sweeps, 1E-12)

    energy, psi = dmrg(H, psi0, sweeps)

    @show measure_entropy!(psi, 1)
    @show measure_spin!(psi)
    # @show psi.data
end

main()