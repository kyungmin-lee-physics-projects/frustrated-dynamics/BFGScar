using LatticeTools

#export make_kagome_lattice

function make_kagome_lattice(size_matrix ::AbstractMatrix{<:Integer}; compute_symmetry::Bool=false)
    latticevectors = [1 -0.5; 0 0.5*sqrt(3.0)];
    unitcell = make_unitcell(latticevectors, SiteType=String)
    addsite!(unitcell, "A", carte2fract(unitcell, [0.5, 0.0]))
    addsite!(unitcell, "B", carte2fract(unitcell, [0.25, 0.25*sqrt(3.0)]))
    addsite!(unitcell, "C", carte2fract(unitcell, [0.5+0.25, 0.25*sqrt(3.0)]))

    nnbondtypes = [
        ([ 0, 0], "A", [ 0, 0], "B", 1),
        ([ 0, 0], "B", [ 0, 0], "C", 1),
        ([ 0, 0], "C", [ 0, 0], "A", 1),

        # ([ 0, 1], "A", [ 1, 0], "B",-1),
        # ([ 1, 0], "B", [ 0, 0], "C",-1),
        # ([ 0, 0], "C", [ 0, 1], "A",-1),

        ([ 1, 1], "A", [ 1, 0], "B",-1),
        ([ 1, 0], "B", [ 0, 0], "C",-1),
        ([ 0, 0], "C", [ 1, 1], "A",-1),
    ]

    nnnbondtypes = [
        ([ 0, 0], "A", [ 1, 0], "B", 1), # ◁
        # ([ 1, 0], "B", [ 1,-1], "C", 1),
        ([ 1, 0], "B", [ 0,-1], "C", 1),
        # ([ 1,-1], "C", [ 0, 0], "A", 1),
        ([ 0,-1], "C", [ 0, 0], "A", 1),
        ([ 0, 0], "C", [ 1, 0], "A",-1),
        # ([ 1, 0], "A", [ 1,-1], "B",-1), # ▷
        ([ 1, 0], "A", [ 0,-1], "B",-1), # ▷
        # ([ 1,-1], "B", [ 0, 0], "C",-1),
        ([ 0,-1], "B", [ 0, 0], "C",-1),
    ]

    lattice = make_lattice(unitcell, size_matrix)
    hypercube = lattice.hypercube
    supercell = lattice.supercell
    tsym = TranslationSymmetry(lattice)
    psym = little_symmetry(tsym, project(PointSymmetryDatabase.get(25), [1 0 0; 0 1 0]))
    tsymbed = embed(lattice, tsym)
    psymbed = embed(lattice, psym)
    ssymbed = tsymbed ⋊ psymbed

    nnbonds = []
    nnnbonds = []

    for r in lattice.bravais_coordinates
        for (rowvec, rowsite, colvec, colsite, bondsign) in nnbondtypes
            R_row, r_row = hypercube.wrap(r .+ rowvec)
            R_col, r_col = hypercube.wrap(r .+ colvec)
            rowsite_super = (rowsite, r_row)
            colsite_super = (colsite, r_col)
            irow = get(supercell.siteindices, rowsite_super, -1)
            icol = get(supercell.siteindices, colsite_super, -1)
            push!(nnbonds, ((irow, icol), R_col-R_row, bondsign))
        end
        for (rowvec, rowsite, colvec, colsite, bondsign) in nnnbondtypes
            R_row, r_row = hypercube.wrap(r .+ rowvec)
            R_col, r_col = hypercube.wrap(r .+ colvec)
            rowsite_super = (rowsite, r_row)
            colsite_super = (colsite, r_col)
            irow = get(supercell.siteindices, rowsite_super, -1)
            icol = get(supercell.siteindices, colsite_super, -1)
            push!(nnnbonds, ((irow, icol), R_col-R_row, bondsign))
        end
    end

    nn_triangles = []
    for r in lattice.bravais_coordinates
      triangle = []
      for (rowvec, rowsite, colvec, colsite, bondsign) in nnbondtypes[1:3]
        R_row, r_row = hypercube.wrap(r .+ rowvec)
        R_col, r_col = hypercube.wrap(r .+ colvec)
        rowsite_super = (rowsite, r_row)
        colsite_super = (colsite, r_col)
        irow = get(supercell.siteindices, rowsite_super, -1)
        icol = get(supercell.siteindices, colsite_super, -1)
        push!(triangle, ((irow, icol), R_col-R_row))
      end
      push!(nn_triangles, (triangle, 1))

      triangle = []
      for (rowvec, rowsite, colvec, colsite, bondsign) in nnbondtypes[4:6]
        R_row, r_row = hypercube.wrap(r .+ rowvec)
        R_col, r_col = hypercube.wrap(r .+ colvec)
        rowsite_super = (rowsite, r_row)
        colsite_super = (colsite, r_col)
        irow = get(supercell.siteindices, rowsite_super, -1)
        icol = get(supercell.siteindices, colsite_super, -1)
        push!(triangle, ((irow, icol), R_col-R_row))
      end
      push!(nn_triangles, (triangle, -1))
    end


    nnn_triangles = []
    for r in lattice.bravais_coordinates
      triangle = []
      for (rowvec, rowsite, colvec, colsite, bondsign) in nnnbondtypes[1:3]
        R_row, r_row = hypercube.wrap(r .+ rowvec)
        R_col, r_col = hypercube.wrap(r .+ colvec)
        rowsite_super = (rowsite, r_row)
        colsite_super = (colsite, r_col)
        irow = get(supercell.siteindices, rowsite_super, -1)
        icol = get(supercell.siteindices, colsite_super, -1)
        push!(triangle, ((irow, icol), R_col-R_row))
      end
      push!(nnn_triangles, (triangle, 1))

      triangle = []
      for (rowvec, rowsite, colvec, colsite, bondsign) in nnnbondtypes[4:6]
        R_row, r_row = hypercube.wrap(r .+ rowvec)
        R_col, r_col = hypercube.wrap(r .+ colvec)
        rowsite_super = (rowsite, r_row)
        colsite_super = (colsite, r_col)
        irow = get(supercell.siteindices, rowsite_super, -1)
        icol = get(supercell.siteindices, colsite_super, -1)
        push!(triangle, ((irow, icol), R_col-R_row))
      end
      push!(nnn_triangles, (triangle, -1))
    end


    # new implementations

    motifs = let
        up_triangle_types = [
            ("A", [ 1, 1]), ("B", [ 1, 0]), ("C", [ 0, 0]),
        ]
        down_triangle_types = [
            ("A", [ 0, 0]), ("B", [ 0, 0]), ("C", [ 0, 0]),
        ]
        hexagon_types = [
            ("A", [ 0, 0]),
            ("B", [ 0,-1]),
            ("C", [ 0,-1]),
            ("A", [ 1, 0]),
            ("B", [ 1, 0]),
            ("C", [ 0, 0]),
        ]

        up_triangles = Vector{Tuple{Int, Vector{Int}}}[]
        down_triangles = Vector{Tuple{Int, Vector{Int}}}[]
        hexagons = Vector{Tuple{Int, Vector{Int}}}[]

        for ucvec in lattice.bravais_coordinates
            let m = Tuple{Int, Vector{Int}}[]
                for (sitetype, sitevec) in up_triangle_types
                    R, r = hypercube.wrap(ucvec .+ sitevec)
                    site_super = (sitetype, r)
                    isite = getsiteindex(supercell, site_super)
                    push!(m, (isite, R))
                end
                push!(up_triangles, m)
            end

            let m = Tuple{Int, Vector{Int}}[]
                for (sitetype, sitevec) in down_triangle_types
                    R, r = hypercube.wrap(ucvec .+ sitevec)
                    site_super = (sitetype, r)
                    isite = getsiteindex(supercell, site_super)
                    push!(m, (isite, R))
                end
                push!(down_triangles, m)
            end

            let m = Tuple{Int, Vector{Int}}[]
                for (sitetype, sitevec) in hexagon_types
                    R, r = hypercube.wrap(ucvec .+ sitevec)
                    site_super = (sitetype, r)
                    isite = getsiteindex(supercell, site_super)
                    push!(m, (isite, R))
                end
                push!(hexagons, m)
            end
        end
        (up_triangles=up_triangles,
         down_triangles=down_triangles,
         hexagons=hexagons)
    end

    #=
    nn_triangles = []
    for r in lattice.bravais_coordinates
      triangle = []
      for (rowvec, rowsite, colvec, colsite, bondsign) in nnbondtypes[1:3]
        R_row, r_row = hypercube.wrap(r .+ rowvec)
        R_col, r_col = hypercube.wrap(r .+ colvec)
        rowsite_super = (rowsite, r_row)
        colsite_super = (colsite, r_col)
        irow = get(supercell.siteindices, rowsite_super, -1)
        icol = get(supercell.siteindices, colsite_super, -1)
        push!(triangle, ((irow, icol), R_col-R_row))
      end
      push!(nn_triangles, (triangle, 1))

      triangle = []
      for (rowvec, rowsite, colvec, colsite, bondsign) in nnbondtypes[4:6]
        R_row, r_row = hypercube.wrap(r .+ rowvec)
        R_col, r_col = hypercube.wrap(r .+ colvec)
        rowsite_super = (rowsite, r_row)
        colsite_super = (colsite, r_col)
        irow = get(supercell.siteindices, rowsite_super, -1)
        icol = get(supercell.siteindices, colsite_super, -1)
        push!(triangle, ((irow, icol), R_col-R_row))
      end
      push!(nn_triangles, (triangle, -1))
    end
    =#

    return (unitcell=unitcell,
            lattice=lattice,
            space_symmetry_embedding=ssymbed,
            nearest_neighbor_bonds=nnbonds,
            next_nearest_neighbor_bonds=nnnbonds,
            nearest_neighbor_triangles=nn_triangles,
            next_nearest_neighbor_triangles=nnn_triangles,
            motifs=motifs)

end





function parse_shape(shape_str::AbstractString)
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"
    m = match(shape_pattern, shape_str)
    isnothing(m) && throw(ArgumentError("shape should be in format (n11,n12)x(n21,n22)"))
    n11, n12, n21, n22 = [parse(Int, x) for x in m.captures]
    return [n11 n21; n12 n22]
end
